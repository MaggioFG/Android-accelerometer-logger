package it.capado.datalogger

/**
 * @author m.parisi6@studenti.poliba.it
 * @author f.doria1@studenti.poliba.it
 * @author f.capezzera@studenti.poliba.it
 *
 */

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Environment.DIRECTORY_DOCUMENTS
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.RuntimeExecutionException
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.*
import java.util.Calendar.*

class MainActivity : AppCompatActivity(), SensorEventListener {
    private var deviceName = Build.MODEL
    var now = getInstance()
    var day: Int = now.get(DAY_OF_MONTH)
    var month: Int = now.get(MONTH) + 1
    var year: Int = now.get(YEAR)
    var fileName = "$deviceName sensed values - $day-$month-$year.csv"
    val csv_header = "device_name,timestamp,x,y,z,lat,long,event,speed\n"


    private lateinit var mSensorManager: SensorManager
    private var mSensors: Sensor? = null

    private val PERMISSION_ID = 42
    private val KEY_SAVE_FILE_TOGGLE = "saveFile_toggle"
    private val KEY_UDP_TOGGLE = "udp_toggle"
    private val KEY_SENSING_TOGGLE = "sensing_toggle"
    private val KEY_IP_ADDRESS = "ip_address"
    private val KEY_PORT_NUMBER = "port"


    var sensing_ison: Boolean = false
    var saveFile_ison: Boolean = false
    var udp_ison: Boolean = false
    var headset_pressed: Boolean = false

    private var sensedValues: StringBuilder = StringBuilder()
    private var hasPending: Boolean = false

    private val mainTimer = Timer()
    private var lat = "NONE"
    private var lon = "NONE"
    private var speed = "NONE"
    private var host: String = "francescocapezzera.tk"
    private var port: Int = 5555

    private var old_ts = System.currentTimeMillis()

    var fusedLocationClient: FusedLocationProviderClient? = null

    val reqSetting = LocationRequest.create().apply {
        fastestInterval = 1000
        interval = 1000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//        smallestDisplacement = 1.0f
    }

    val REQUEST_CHECK_STATE = 12300 // any suitable ID

    val builder = LocationSettingsRequest.Builder()
        .addLocationRequest(reqSetting)


    val locationUpdates = object : LocationCallback() {
        override fun onLocationResult(lr: LocationResult) {
            Log.e("LOG", lr.toString())
            Log.e("LOG", "Newest Location: " + lr.locations.last())
            lat = lr.locations.last().latitude.toString()
            lon = lr.locations.last().longitude.toString()
            if (lr.locations.last().hasSpeed()) {
//                speed = lr.locations.last().speed.toString()
                speed = String.format("%.3f", lr.locations.last().speed).replace(",", ".")
            } else {
                speed = "NONE"
            }
            Log.d("SPEED", speed)
        }
    }


    private fun checkPermission(vararg perm: String): Boolean {
        val havePermissions = perm.toList().all {
            ContextCompat.checkSelfPermission(this, it) ==
                    PackageManager.PERMISSION_GRANTED
        }
        if (!havePermissions) {
            if (perm.toList().any {
                    ActivityCompat.shouldShowRequestPermissionRationale(this, it)
                }
            ) {
                val dialog = AlertDialog.Builder(this)
                    .setTitle("Permission")
                    .setMessage("Permission needed!")
                    .setPositiveButton("OK") { _, _ ->
                        ActivityCompat.requestPermissions(
                            this, perm, PERMISSION_ID
                        )
                    }
                    .setNegativeButton("No") { _, _ -> }
                    .create()
                dialog.show()
            } else {
                ActivityCompat.requestPermissions(this, perm, PERMISSION_ID)
            }
            return false
        }
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_UDP_TOGGLE, udp_ison)
        outState.putBoolean(KEY_SENSING_TOGGLE, sensing_ison)
        outState.putBoolean(KEY_SAVE_FILE_TOGGLE, saveFile_ison)
        outState.putString(KEY_IP_ADDRESS, host)
        outState.putInt(KEY_PORT_NUMBER, port)
        Log.i("save_instance_state", "Saving state UDP: $udp_ison, SENSING : $sensing_ison")
    }


    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent?) {

        if (event!!.sensor.type == Sensor.TYPE_ACCELEROMETER) {

            if (sensing_ison) {

                val x = event.values[0]
                val y = event.values[1]
                val z = event.values[2]
                val currentTimestamp = System.currentTimeMillis()

                val tsdiff = currentTimestamp - old_ts
                old_ts = currentTimestamp
                if (tsdiff < 1) return

//            Log.i("LOOK AT ME", "$tsdiff")


                val toSend =
                    "$deviceName,$currentTimestamp,$x,$y,$z,$lat,$lon,$headset_pressed,$speed"
//                Log.i("FC_SENSORCHANGE", toSend)
                //Log.d("event_trigger", "APPENDING: $headset_pressed")
                sensedValues.append("$toSend\n")
                hasPending = true
                //Log.i("last_sensed_value", "$currentTimestamp")

                if (!udp_ison) return
                Thread {
                    send(host = host, port = this.port, data = toSend.toByteArray())
                }.start()

            }
        }


    }

    fun send(
        host: String = "",
        port: Int = 5555,
        data: ByteArray,
        senderPort: Int = 5555
    ): Boolean {
        var ret = false
        var socket: DatagramSocket? = null
        try {
            socket = DatagramSocket(senderPort)
            val address = InetAddress.getByName(host)
            val packet = DatagramPacket(data, data.size, address, port)
            socket.send(packet)
            ret = true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            socket?.close()
        }
        return ret
    }

    override fun onStop() {
        super.onStop()
        if (saveFile_ison && hasPending) {
            if (writeCsv()) {
                val toast =
                    Toast.makeText(applicationContext, "Saving CSV file", Toast.LENGTH_LONG)
                toast.show()
                Log.d("write_csv", "Writing csv onStop")
            } else {
                val toast =
                    Toast.makeText(applicationContext, "Error writing FILE!", Toast.LENGTH_LONG)
                toast.show()
                Log.e("write_csv", "ERROR Writing csv onStop")

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (saveFile_ison && hasPending) {
            if (writeCsv()) {
                val toast =
                    Toast.makeText(applicationContext, "Saving CSV file", Toast.LENGTH_LONG)
                toast.show()
                Log.d("write_csv", "Writing csv onDestroy")
            } else {
                val toast =
                    Toast.makeText(applicationContext, "Error writing FILE!", Toast.LENGTH_LONG)
                toast.show()
                Log.e("write_csv", "ERROR Writing csv onDestroy")

            }
        }
        //mSensorManager.unregisterListener(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (savedInstanceState != null) {
            udp_ison = savedInstanceState.getBoolean(KEY_UDP_TOGGLE, false)
            sensing_ison = savedInstanceState.getBoolean(KEY_SENSING_TOGGLE, false)
            saveFile_ison = savedInstanceState.getBoolean(KEY_SAVE_FILE_TOGGLE, false)

            host = savedInstanceState.getString(KEY_IP_ADDRESS, "francescocapezzera.tk")
            port = savedInstanceState.getInt(KEY_PORT_NUMBER, 5555)
            Log.i("save_instance_state", "recovered state UDP: $udp_ison, SENSING : $sensing_ison")
        }


        checkPermission(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val client = LocationServices.getSettingsClient(this)

        client.checkLocationSettings(builder.build()).addOnCompleteListener { task ->
            try {
                val state: LocationSettingsStates = task.result!!.locationSettingsStates
                Log.e(
                    "LOG", "LocationSettings: \n" +
                            " GPS present: ${state.isGpsPresent} \n" +
                            " GPS usable: ${state.isGpsUsable} \n" +
                            " Location present: " +
                            "${state.isLocationPresent} \n" +
                            " Location usable: " +
                            "${state.isLocationUsable} \n" +
                            " Network Location present: " +
                            "${state.isNetworkLocationPresent} \n" +
                            " Network Location usable: " +
                            "${state.isNetworkLocationUsable} \n"
                )
            } catch (e: RuntimeExecutionException) {
                if (e.cause is ResolvableApiException) {
                    (e.cause as ResolvableApiException).startResolutionForResult(
                        this@MainActivity,
                        REQUEST_CHECK_STATE
                    )
                }
            }

        }

        fusedLocationClient?.requestLocationUpdates(
            reqSetting,
            locationUpdates,
            null /* Looper */
        )


        //IP Address Text
        val sharedPref: SharedPreferences = getSharedPreferences("my_prefs", 0)

        host = sharedPref.getString("hostname", "francescocapezzera.tk").toString()

        val editTextIP = findViewById<EditText>(R.id.editText_ip)
        Log.d("FC_EDITTEXTIP", editTextIP.text.toString())
        editTextIP.setText(host)
        editTextIP.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                Log.d("FC_EDITTEXTIP", editTextIP.text.toString())
                host = editTextIP.text.toString()

                val editor = sharedPref.edit()
                editor.putString("hostname", host)
                editor.apply()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        //UDP Port number text
        port = sharedPref.getInt("port", 5555)

        val editTextPort = findViewById<EditText>(R.id.editText_port)
        Log.d("FC_EDITTEXT_PORT", editTextPort.text.toString())
        editTextPort.setText(port.toString())
        editTextPort.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                Log.d("FC_EDITTEXTIP", editTextPort.text.toString())
                port = editTextPort.text.toString().toInt()

                val editor = sharedPref.edit()
                editor.putInt("port", port)
                editor.apply()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })


        // BUTTON SHARE
        val shareButton = findViewById<ImageButton>(R.id.button_share)
        shareButton.setOnClickListener {
            Log.d("UI_DEBUG", "Button SHARE pressed")
            try {


                val dirName = "sensed_data"

                val dirPath = if (Build.VERSION.SDK_INT >= 19) {
                    Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS)
                        .absolutePath
                } else {
                    Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS)
                        .absolutePath
                }
                val baseDir = File(dirPath + File.separator + dirName)

                val file = File(baseDir, fileName)

                val intent = Intent(Intent.ACTION_SEND)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                val prova = FileProvider.getUriForFile(
                    getApplicationContext(),
                    getPackageName() + ".fileprovider",
                    file
                )
                intent.putExtra(Intent.EXTRA_STREAM, prova)
                intent.type = "text/csv"
                startActivity(Intent.createChooser(intent, "Share text via"))

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        val tb_udp: Switch = findViewById(R.id.toggleButton_udp)
        tb_udp.isChecked = sharedPref.getBoolean("udp", false)
        udp_ison = tb_udp.isChecked
        tb_udp.setOnCheckedChangeListener { _, isChecked ->
            Log.d("UI_debug", isChecked.toString())
            udp_ison = isChecked
            val editor = sharedPref.edit()
            editor.putBoolean("udp", isChecked)
            editor.apply()
        }


        val tb_saveFile: Switch = findViewById(R.id.toggleButton_saveFile)
        tb_saveFile.isChecked = sharedPref.getBoolean("saveFile", false)
        saveFile_ison = tb_saveFile.isChecked
        tb_saveFile.setOnCheckedChangeListener { _, isChecked ->
            Log.d("UI_debug", isChecked.toString())

            if (isChecked){
                if(!checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    tb_saveFile.isChecked = false
                }
            }
            saveFile_ison = isChecked
            val editor = sharedPref.edit()
            editor.putBoolean("saveFile", isChecked)
            editor.apply()
            if (!isChecked && hasPending){
                if (writeCsv()) {
                    val toast =
                        Toast.makeText(applicationContext, "Saving CSV file", Toast.LENGTH_LONG)
                    toast.show()
                    Log.d("write_csv", "Writing csv on SavingFile OFF")
                } else {
                    val toast =
                        Toast.makeText(applicationContext, "Error writing FILE!", Toast.LENGTH_LONG)
                    toast.show()
                    Log.e("write_csv", "ERROR Writing csv on SavingFile OFF")

                }
            }
        }

        /*val tb_keepOn: ToggleButton = findViewById<ToggleButton>(R.id.toggleButton_keepOn)
        tb_keepOn.setOnCheckedChangeListener { _, isChecked ->
            Log.d("UI_debug", isChecked.toString())
            if (isChecked) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            } else {
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }*/

        val tb_sensing: Switch = findViewById<Switch>(R.id.toggleButton_sensing)
        tb_sensing.setOnCheckedChangeListener { _, isChecked ->
            Log.d("UI_debug", isChecked.toString())
            sensing_ison = isChecked
            if (saveFile_ison && !isChecked && hasPending) {
                if (writeCsv()) {
                    val toast =
                        Toast.makeText(applicationContext, "Saving CSV file", Toast.LENGTH_LONG)
                    toast.show()
                    Log.d("write_csv", "Writing csv on Sensing OFF")
                } else {
                    val toast =
                        Toast.makeText(applicationContext, "Error writing FILE!", Toast.LENGTH_LONG)
                    toast.show()
                    Log.e("write_csv", "ERROR Writing csv on Sensing OFF")

                }
            }
        }



        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val deviceSensors: List<Sensor> = mSensorManager.getSensorList(Sensor.TYPE_ALL)
        Log.v("Total sensors", "" + deviceSensors.size)
        deviceSensors.forEach {
            Log.v("Sensor name", "" + it)
        }

        mSensors = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mSensorManager.registerListener(this, mSensors, SensorManager.SENSOR_DELAY_FASTEST)

        val task = object : TimerTask() {
            override fun run() {
                if (saveFile_ison && hasPending) {
                    if (writeCsv()) {
                        Log.d("write_csv", "Writing in mainTimer")
                    } else {
                        Log.e("write_csv", "ERROR writing csv in mainTimer")
                    }
                }
            }
        }
        mainTimer.schedule(task, 10000, 30000)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
/*
    override fun onActivityResult() {
        super.onActivityResult()
    }*/
/*

    @Throws (IOException::class)
    fun createFile(mimeType: String, file:String) : Boolean {
        ret = false
            if (isExternalStorageWritable()) {
                val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                    addCategory(Intent.CATEGORY_OPENABLE)
                    type = mimeType
                    putExtra(Intent.EXTRA_TITLE, file)
                }
                startActivityForResult(intent, WRITE_REQUEST_CODE)
                ret = true
            }
        return ret
    }
*/


    private fun writeCsv(): Boolean {
        var ret = false
        val fw: FileWriter
        val dirName = "sensed_data"
        //fileName = "$deviceName sensed values - $day-$month-$year.csv"

        try {
            if (isExternalStorageWritable()) {
                Log.d("External_memory_status", "Is_Writable")


                val dirPath = if (Build.VERSION.SDK_INT >= 19) {
                    Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS)
                        .absolutePath
                } else {
                    Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS)
                        .absolutePath
                }

                val baseDir = File(dirPath + File.separator + dirName)

                if (!baseDir.exists()) {
                    if (!baseDir.mkdirs()) {
                        Log.e("write_csv", "Could not create directories $baseDir")
                    }
                }


                val file = File(baseDir, fileName)
                fw = FileWriter(file, true)
                if (file.length() == 0.toLong()) {
                    fw.append(csv_header)
                }

                val writer = PrintWriter(fw, true)
                val lines = sensedValues.toString()
//            var lines = sensedValues.toString().split("\n")
//            lines.forEach {
//                writer.println(it)
//            }
                writer.print(lines)
                sensedValues.setLength(0)
                fw.flush()
                fw.close()
                writer.flush()
                writer.close()
                hasPending = false
                ret = true
            }

        } catch (e: IOException) {
            Log.e("write_csv", "Catched IOException in writeCSV()" + e.printStackTrace().toString())
        }

        return ret
    }

    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
            //Log.d("event_trigger", "Headset button pressed")
            headset_pressed = true
            //Log.d("event_trigger", "Changing headset_pressed to TRUE")

            val timer = Timer()
            timer.schedule(object : TimerTask() {

                override fun run() {
                    //This is where we tell it what to do when it hits 20 milliseconds
                    //Log.d("event_trigger", "Changing headset_pressed to FALSE")
                    headset_pressed = false
                }
            }, 100)

            if (sensing_ison) {
                val toast =
                    Toast.makeText(applicationContext, "Event Triggered", Toast.LENGTH_SHORT)
                toast.show()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}

